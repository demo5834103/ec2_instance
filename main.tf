provider "aws" {
  region = "eu-west-2"

}

resource "aws_instance" "web_instance" {
  ami           = "ami-0b7c168be68b67913"
  instance_type = "t2.micro"

  user_data = <<-EOF
              #!/bin/bash
              exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
              set -x
              git clone https://gitlab.com/demo5834103/my-portfolio.git website
              sudo cp -r website/* /var/www/html/
              sudo systemctl restart apache2
              systemctl status apache2
              curl http://169.254.169.254/latest/meta-data/public-ipv4 > /var/www/html/public_ip.txt
              EOF
  tags = {
    Name = "portfolio"
  }
}

